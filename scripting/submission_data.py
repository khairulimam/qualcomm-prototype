from bs4 import BeautifulSoup
import requests

submission_html = requests.get('https://developforai.com/submissions').content
parser = BeautifulSoup(submission_html, 'html.parser')

submitted = parser.find_all('div', {'class': 'c-submission'})

submission = list()
content = ''
for i, s in enumerate(submitted, 1):
  meta = s.find('div', {'class': 'c-submission__meta'})
  overlay = s.find('div', {'class': 'c-submission__overlay'})
  submitter = meta.find('span').text.encode('utf-8')
  title = meta.find('h3').text.encode('utf-8').strip()
  description = overlay.find('p').text.encode('utf-8').strip()
  img = s.find('img').get('src').encode('utf-8').strip()
  submission.append({'submitter': submitter, 'title': title, 'description': description, 'image': img})
  content += 'No: {}\nSubmitter: {}\nTitle: {}\nDescription: {}\nImage: {}\n############\n'.format(i, submitter, title, description, img)
  

if __name__ == '__main__':
  print content