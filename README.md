# INSTALL
`pip install -r requirements.txt`

## SEE SUBMITTED IDEA
`python submission_data.py`

## SAVE SUBMITTED TO TXT
`python totxt.py`

## SAVE SUBMITTED TO JSON
`python tojson.py`

## SAVE SUBMITTED TO CSV
`python tocsv.py`

